﻿
namespace BitFeeder
{
   
    public sealed class FeedItem
    {
        public string Title { get; set; }
        public string Date { get; set; }
        public string DirectLink { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
        public string SourceFeed { get; set; }
        public string Category { get; set; }
        public string ReadState { get; set; }

        public override string ToString()
        {
            return 
                "Title "        + this.Title        + "|" +
                "Date "         + this.Date         + "|" +
                "Link "         + this.DirectLink   + "|" +
                "DirectLink "   + this.Description  + "|" + 
                "GUID "         + this.Guid         + "|" +
                "SourceFeed "   + this.SourceFeed   + "|" + 
                "Cateogry "     + this.Category     + "|" +
                "ReadState "    + this.ReadState;
        }
     
    }

}