﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

/// <remaks>
///     Author: corneliustodd@gmail.com
/// </remarks>

namespace BitFeeder
{
    /// <summary>
    /// BitFeeder specific utilities for db interface
    /// </summary>
    sealed class BitFeedDB
    {
        private SQLiteDatabase db;

        private static List<string> categories = new List<string>(Properties.Settings.Default.categories.Split(new char[] { ',' }));
        public static readonly List<String> _emptyData = new List<String>();
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Public Methods
        /// <summary>
        ///     Default constructor for BitFeedDB
        /// 
        /// Meh... perhaps this should not be run every..single... time... we instantiate the db, eh?
        /// 
        /// </summary>
        public BitFeedDB() 
        {
            try
            {
                String init = 
                    "BEGIN TRANSACTION; " +
                    " CREATE TABLE IF NOT EXISTS [Category] ( [c_uid] integer PRIMARY KEY NOT NULL, [f_uid] integer NOT NULL, [c_push] " +
                    " numeric NOT NULL DEFAULT 1, [c_strip] numeric NOT NULL DEFAULT 1, [c_fork] numeric NOT NULL DEFAULT 0, [c_delete] numeric NOT NULL DEFAULT 1, " +
                    " [c_issue] numeric NOT NULL DEFAULT 1, [c_request] numeric NOT NULL DEFAULT 1, [c_merge] numeric NOT NULL DEFAULT 1, [c_follow] numeric NOT NULL " +
                    " DEFAULT 0, [c_create] numeric NOT NULL DEFAULT 0, [c_commit] numeric NOT NULL DEFAULT 0, [c_upload] numeric NOT NULL DEFAULT 1, [c_misc] numeric " +
                    " NOT NULL DEFAULT 1 );" +
                    " CREATE TABLE IF NOT EXISTS [Config] (  [c_speed] numeric NOT NULL DEFAULT 5,  [c_replayTimer] numeric NOT NULL DEFAULT 5,  " +
                    " [c_alwaysOnTop] numeric NOT NULL DEFAULT 1, [c_startOnLogin] numeric NOT NULL DEFAULT 1, [c_uid] integer PRIMARY KEY NOT NULL  );" +
                    " CREATE TABLE IF NOT EXISTS [Feed] (  [f_uid] integer PRIMARY " +
                    " KEY NOT NULL,  [f_user] text,  [f_pass] text,  [f_url] text NOT NULL,  [f_lastchecked] text NOT NULL  ); " +
                    " CREATE TABLE IF NOT EXISTS [FeedItem] ( " +
                    " [i_uid] integer PRIMARY KEY, [f_uid] integer NOT NULL, [i_title] text, [i_pubdate] text, [i_link] text, [i_description] text, [i_category] text, " +
                    " [i_read] text NOT NULL, [i_rssid] text NOT NULL ); COMMIT; ";

                db = new SQLiteDatabase();
                db.ExecuteNonQuery(init);

                log.Info("Successfully initialized database");

                //Check for null config
                //We only have a single config so c_uid == 1 should fail with an exception
                // should there already be a config entry.
                try
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data.Add("c_speed", "5");
                    data.Add("c_replayTimer", "5");
                    data.Add("c_alwaysOnTop", "1");
                    data.Add("c_startOnLogin", "1");

                    if (db.ExecuteScalar("select c_uid from Config where c_uid = 1") == "")
                        db.Insert("Config", data);

                    log.Info("A default config entry was created");
                }
                catch (Exception)
                {
                    log.Info("The database already contains a config entry");
                }

            }
            catch (Exception ex1)
            {
                log.Fatal("Error initializing database", ex1);
            }
        }

        /// <summary>
        ///     Helper routine for insterting a new Feed into database
        /// </summary>
        /// <param name="feed">Feed object to persist</param>
        public void AddNewFeed(Feed feed)
        {
            log.Debug("AddNewFeed:feed:: " + feed);

            db = new SQLiteDatabase();
            try
            {
                //Encode before storage
                feed.Url = toBase64(feed.Url);
                db.Insert("Feed", feed.FeedParamsToDict());
            }
            catch (Exception ex1)
            {
                log.Fatal("Error adding new feed", ex1);
                log.Fatal("Feed: " + feed.ToString());
            }
        }

        /// <summary>
        ///     Helper function for inserting a new FeedItem. Certain values are base64 encoded before storage
        /// </summary>
        /// <param name="item">The raw feed item to store</param>
        public void AddNewFeedItem(FeedItem item)
        {
            log.Debug("AddNewFeedItem:item:: " + item.ToString());

            Dictionary<String, String> data = new Dictionary<String, String>();
            data.Add("f_uid", item.SourceFeed);
            data.Add("i_title", item.Title);
            data.Add("i_pubdate", item.Date);
            data.Add("i_link", item.DirectLink);
            data.Add("i_description", item.Description);
            data.Add("i_category", item.Category);
            data.Add("i_read", "1".ToString());
            data.Add("i_rssid", item.Guid);
            try
            {
                db.Insert("FeedItem", data);
            }
            catch (Exception ex1)
            {
                log.Fatal("Error adding new feed item", ex1);
                log.Fatal("FeedItem" + item.ToString());
            }

           
        }



        /// <summary>
        ///     Helper function for marking a single item as read. Read items are not longer shown across the feeder.
        /// </summary>
        /// <param name="item">The FeedItem to be marked as read</param>
        public void MarkAsRead(FeedItem item)
        {
            log.Debug("MarkAsRead:item:: " + item.ToString());

            Dictionary<String, String> data = new Dictionary<string, string>();

            //1 == read, 0 == unread
            data.Add("i_read", "1");
            try
            {
                db.Update("FeedItem", data, String.Format("FeedItem.i_rssid = {0}", item.Guid));
            }
            catch (Exception ex1)
            {
                log.Fatal("Error marking feed as read", ex1);
            }

        }

        /// <summary>
        ///     Helper function to delete a feed from the database
        /// </summary>
        /// <param name="url">The url for the feed</param>
        public void DeleteFeed(String url)
        {
            log.Debug("DeleteFeed:url:: " + url);

            try
            {
                String query = "SELECT f_uid \"f_uid\" FROM Feed";

                log.Debug("DeleteFeed:query:: " + query);
                DataTable feed = db.GetDataTable(query);

                db.Delete("Feed", String.Format("f_uid = {0}", feed.PrimaryKey));
                db.Delete("FeedItem", String.Format("f_uid = {0}", feed.PrimaryKey));
            }
            catch (Exception ex1)
            {
                log.Fatal("Error deleting feed", ex1);
            }

        }

        /// <summary>
        ///     Helper function to return all feeds from database
        /// </summary>
        /// <returns>A List of strings containing 0 or more Feeds URLs</returns>
        public List<String> GetAllFeeds()
        {
            return innerGetAllFeeds() ?? _emptyData;
           
        }

        /// <summary>
        ///     Inner helper so we are don't provide null Lists to clients
        /// </summary>
        /// <returns></returns>
        private List<string> innerGetAllFeeds()
        {
            List<String> data = null;
            try
            {
                String allQ = "SELECT f_url FROM Feed";

                // Remember to enumerate DataTable before running a linq query against it. Good news, it supports Lamba functions.
                // Also convert back from base64
                data = db.GetDataTable(allQ).AsEnumerable().Select(x => fromBase64(x[0].ToString())).ToList();

            }
            catch (Exception ex1)
            {
                log.Fatal("Error getting all feeds", ex1);
            }

            log.Debug("Retrieved all feeds");

            return data;
    
        }

        /// <summary>
        ///     Helper routine to mark ALL feedItems items as READ
        /// </summary>
        public void MarkAllRead() 
        {
            String query = "SELECT i_uid \"i_uid\" FROM FeedItem";
            Dictionary<String, String> data = new Dictionary<string,string>();
            data.Add("f_readState", "1");

            log.Debug("MarkAllRead:data:: " + data);

            try
            {
                DataTable feedItems = db.GetDataTable(query);

                foreach (DataRow r in feedItems.Rows)
                {
                    db.Update("FeedItem", data, "i_uid > 0");
                }
            }
            catch (Exception ex1)
            {
                log.Fatal("Error marking ALL as read", ex1);
            }

        }

        /// <summary>
        ///     Helper routine to mark ALL feedItems as UNREAD
        /// </summary>
        public void MarkAllUnRead() 
        {
            String query = "SELECT i_uid \"i_uid\" FROM FeedItem";
            Dictionary<String, String> data = new Dictionary<string, string>();
            data.Add("f_readState", "0");

            log.Debug("MarkAllUnRead:data:: " + data);

            try
            {
                DataTable feedItems = db.GetDataTable(query);

                foreach (DataRow r in feedItems.Rows)
                {
                    db.Update("FeedItem", data, "i_uid > 0");
                }
            }
            catch (Exception ex1)
            {
                log.Fatal("Error marking all as unread", ex1);
            }
        }

        /// <summary>
        ///     Helper function to update which categories the user wants displayed on the feeder.
        ///     TODO: Create category class so only one Dict. is passed
        /// </summary>
        /// <param name="data">The Dictionary contains keys and attribute values for the category entry</param>
        public void UpdateCategory(Dictionary<String, String> data) 
        {

            log.Debug("UpdateCategory:data:: " + data);

            // Try to insert or update the config data. At this time we only want
            // want a single config, not a per feed config
            try
            {
                if (db.ExecuteScalar("select c_uid from Category where c_uid = 1") == "")
                    db.Insert("Category", data);
                else
                    db.Update("Category", data, "c_uid = 1");
            }
            catch (Exception ex1)
            {
                log.Fatal("There was an error updating category E/D", ex1);
            }
        }

        /// <summary>
        ///     Helper function to update the general configuration for BitFeeder
        ///     TODO: Create a config class
        /// </summary>
        /// <param name="data">Dictionary contains keys and attribues describing configuration</param>
        public void UpdateConfig(Dictionary<String, String> data)
        {

            log.Debug("UpdateConfig:data:: " + data);

            // Try to insert or update the config data. At this time we only want
            // want a single config, not a per feed config
            try
            {
                if (db.ExecuteScalar("select c_uid from Config where c_uid = 1") == "")
                    db.Insert("Config", data);
                else
                    db.Update("Config", data, "c_uid = 1");
            }
            catch (Exception ex1)
            {
                log.Fatal("Error updating config", ex1);
            }
        }

        internal bool FeedExists(string p)
        {
            log.Debug("Testing if feed exists " + p);
            p = toBase64(p);
            
            //Note the single quotes around the now encoded url. Also notice the spacing...
            String query = String.Format("select {0} from {1} where {2}{3}{4}", "f_url", "Feed", "f_url = '", p, "'");

            try
            {
                String result = db.ExecuteScalar(query);

                if (!(result.Length == 0))
                    return true;
                return false;
            }
            catch (Exception ex1)
            {
                log.Fatal("Error checking if feed, " + p + " exists ", ex1);
                return false;
            }

        }

        /// <summary>
        /// Determine if the given uid for the FeedItem has already been processed.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool IsStaleItem(string date, string description)
        {
            bool isStale = true;

            log.Info("Testing if feed items is stale " + description);

            //Note the single quotes around the now encoded url. Also notice the spacing...
            String query = String.Format("select {0} from {1} where {2}'{3}' AND {4}'{5}'", "i_uid", "FeedItem", "i_pubdate=", date, "i_description=", description);

            try
            {
                String result = db.ExecuteScalar(query);

                if (result.Length == 0)
                    isStale = false;
            }
            catch (Exception ex1)
            {
                log.Fatal("Error checking if feed item , " + date + " is stale ", ex1);
                isStale = false;
            }

            return isStale;

        }

        #endregion

        #region Helpers
        private String toBase64(String input)
        {
            
            log.Debug("toBase64:input:: " + input);

            if (input == null || input.Length == 0)
                return "";
            byte[] toEncodeAsBytes
               = System.Text.ASCIIEncoding.ASCII.GetBytes(input);
            string returnValue
                  = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
            //return input;

        }

        private String fromBase64(String input)
        {
            log.Debug("fromBase64:input:: " + input);

            if (input == null || input.Length == 0)
                return "";
            byte[] raw
               = Convert.FromBase64String(input);
            string returnValue
                  = Encoding.UTF8.GetString(raw);
            return returnValue;
        }

        public static void Categorize(FeedItem item)
        {
            //DEFAULT
            item.Category = "";


            List<string> data = item.Title.Trim().Trim().Split(' ').ToList<string>();

            // One level categories

            try
            {
                //Compare two lists, decompose to only what is similar
                List<string> result = categories.Intersect(data).ToList<string>();

                //No matches means we had a non-supported feed type
                if (result.Count == 0)
                {
                    item.Category = "MISC";
                }

                //Else the next match, regardless of match count, SHOULD be the proper category
                else
                {
                    item.Category = result[0];
                }
            }
            catch (Exception ex1)
            {
                log.Fatal("Error categorizing item", ex1);
            }

        }

        #region Cleansing

        public static void CleanItem(FeedItem item)
        {
            item.Title = cleanString(item.Title);
            item.DirectLink = cleanString(item.DirectLink);
            item.Description = cleanString(item.Description);
            item.Guid = cleanString(item.Guid);
            item.SourceFeed = cleanString(item.SourceFeed);
        }

        private static string cleanString(string input)
        {
            string output = input.Trim();

            output = output.Replace("&#39;", "'");
            output = output.Replace("&amp;", "&");
            output = output.Replace("&quot;", "\"");
            output = output.Replace("&nbsp;", " ");
            output = output.Replace("\r", "");
            output = output.Replace("\n", "");
            output = output.Replace("\r\n", "");

            output = output.Replace("'", "");
            output = output.Replace("`", "");

            output = removeHTMLTags(output);

            return output;
        }

        private static string removeHTMLTags(string text)
        {
            const string regularExpressionString = "<.+?>";

            Regex r = new Regex(regularExpressionString, RegexOptions.Singleline);
            return r.Replace(text, "");
        }

        #endregion
        #endregion
    }
}
