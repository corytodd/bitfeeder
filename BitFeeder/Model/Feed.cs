﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BitFeeder
{
    /// <summary>
    /// The Feed data that controls each RSS feed for the user. Only a URL is required, the rest is optional. 
    /// </summary>
    public sealed class Feed : INotifyPropertyChanged
    {        
        public Feed() 
        {
            _feedParams = new Dictionary<string, string>();
        }

        #region Feed Members

        private string _user = "";
        private string _pass = "";
        private string _url = "";
        private DateTime _lastChecked = new DateTime();

        private Dictionary<string, bool> _categoryEnable;
        private Dictionary<String, String> _feedParams;


        #endregion

        #region Feed Properties
        
        public string Url
        { 
            get { return this._url; }
            set
            {
                this._url = value;
                NotifyPropertyChanged("Url");
            }
        }
        public DateTime LastChecked 
        {
            get { return this._lastChecked; }
            set
            {
                this._lastChecked = value;
                NotifyPropertyChanged("LastChecked");
            }
        }

        public string Username
        {
            get { return this._user; }
            set
            {
                this._user = value;
                NotifyPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return this._pass; }
            set
            {
                this._pass = value;
                NotifyPropertyChanged("Password");
            }
        }   

        #endregion

        #region Feed Methods

        /// <summary>
        /// Returns this feed item as a dictionary<string, string>.
        /// </summary>
        /// <returns></returns>

        public Dictionary<String, String> FeedParamsToDict()
        {
            _feedParams.Clear();
            _feedParams.Add("f_user", this._user);
            _feedParams.Add("f_pass", this._pass);
            _feedParams.Add("f_url", this._url);
            _feedParams.Add("f_lastChecked", this._lastChecked.ToString());

            return _feedParams;
        }
     
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
