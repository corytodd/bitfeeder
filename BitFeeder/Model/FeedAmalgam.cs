﻿using System;
using System.ComponentModel;

namespace BitFeeder
{
    class FeedAmalgam : INotifyPropertyChanged
    {
        private static volatile FeedAmalgam instance;
        private static object syncRoot = new Object();

        #region Constructor

        public static FeedAmalgam Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new FeedAmalgam();
                    }
                }

                return instance;
            }
        }      


        private FeedAmalgam() { }

        #endregion

        #region Members

        private int _totalFeedCount = 0;

        #endregion

        #region Properties

        public int TotalFeedCount
        {
            get { return _totalFeedCount; }
            set
            {
                _totalFeedCount = value;
                NotifyPropertyChanged("TotalFeedCount");
            }
        }

        #endregion

        #region Private Helpers

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;


        #endregion

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ResponseChanged()
        {

        }

        #endregion
    }
}
