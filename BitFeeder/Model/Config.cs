﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

/// <remaks>
///     Author: corneliustodd@gmail.com
/// </remarks>


namespace BitFeeder
{
    public sealed class Config : INotifyPropertyChanged
    {
        private static volatile Config instance;
        private static object syncRoot = new Object();
        private Dictionary<String, String> data;

        public static Config Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Config();
                    }
                }
                return instance;
            }
        }    

        private Config() 
        {
            data = new Dictionary<string, string>();
        }

        #region Config Members

        private int _scrollSpeed = 5;
        private int _replayTimer = 5;
        private bool _onStartup = true;
        private bool _alwaysOnTop = true;

        #endregion

        #region Config Properties
        public int ScrollSpeed
        {
            get { return this._scrollSpeed; }
            set
            {
                this._scrollSpeed = value;
                NotifyPropertyChanged("ScrollSpeed");
            }
        }

        public int ReplaySpeed
        {
            get { return this._replayTimer; }
            set
            {
                this._replayTimer = value;
                NotifyPropertyChanged("ReplaySpeed");
            }
        }

        public bool AlwaysOnTop
        {
            get { return this._alwaysOnTop; }
            set
            {
                this._alwaysOnTop = value;
                NotifyPropertyChanged("AlwaysOnTop");
            }
        }

        public bool OnStartUp
        {
            get { return this._onStartup; }
            set
            {
                this._onStartup = value;
                NotifyPropertyChanged("OnStartUp");
            }
        }
        #endregion

        #region Config Methods

        public Dictionary<String, String> toDictionary()
        {
            data.Clear();
            data.Add("c_speed", this._scrollSpeed.ToString());
            data.Add("c_replayTimer", this._replayTimer.ToString());
            data.Add("c_onStartUp", this._onStartup.ToString());
            data.Add("c_alwaysOnTop", this._alwaysOnTop.ToString());

            return data;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

}
