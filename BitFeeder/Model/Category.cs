﻿using System.Collections.Generic;

/// <remaks>
///     Author: corneliustodd@gmail.com
/// </remarks>


namespace corytodd.BitFeeder
{
    public sealed class Category
    {
        static readonly Category _instance = new Category();

        private List<string> _list { get; set; }

        //12 total
        private Category()
        {
            _list = new List<string>();
            _list.Add("Pushed");
            _list.Add("Stripped");
            _list.Add("Forked");
            _list.Add("Deleted");
            _list.Add("Issue");
            _list.Add("Request");
            _list.Add("Merge");
            _list.Add("Following");
            _list.Add("Created");
            _list.Add("Comment");
            _list.Add("Uploaded");
            _list.Add("Misc");
        }

        public static Category Instance
        {
            get { return _instance; }
        }

        public List<string> getList()
        {
            return this._list;
        }
       
    }
}
