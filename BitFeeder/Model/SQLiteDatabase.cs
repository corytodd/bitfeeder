﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;

///<summary>
///   Source http://brennydoogles.wordpress.com/2010/02/26/using-sqlite-with-csharp/
///   Though the dreamincode site is more readable, imho
///   
///   And this is actually sourced from http://www.mikeduncan.com/
///   
///   This work is done by the above two users, more or less, with a couple of tweaks to suite my needs. 
///   No license was found in either docs so I'll assume it's free since it is in public domain :/
///</summary>

namespace BitFeeder
{
    sealed class SQLiteDatabase
    {
        String dbConnection;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Default Constructor for SQLiteDatabase Class.
        /// </summary>
        public SQLiteDatabase()
        {
            string dbLocation = Properties.Settings.Default.DatabaseLocation;
            if (String.IsNullOrEmpty(dbLocation))
            {
                try
                {
                    dbLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Properties.Resources.DatabaseFolder);
                    Directory.CreateDirectory(dbLocation);
                    Properties.Settings.Default.DatabaseLocation = dbLocation;

                } catch(Exception ex1)
                {
                    log.Fatal("Failed to create db", ex1);
                }
                 
            }

            string absPathToDb = Path.Combine(dbLocation, Properties.Resources.DatabaseName);
            dbConnection = String.Concat(Properties.Resources.DatabasePrepend, absPathToDb);
        }

        /// <summary>
        ///     Single Param Constructor for specifying the DB file.
        /// </summary>
        /// <param name="inputFile">The File containing the DB</param>
        public SQLiteDatabase(String inputFile)
        {
            log.Info("Setting datasource as " + inputFile);
            dbConnection = String.Format("Data Source={0}", inputFile);
        }

        /// <summary>
        ///     Single Param Constructor for specifying advanced connection options.
        /// </summary>
        /// <param name="connectionOpts">A dictionary containing all desired options and their values</param>
        public SQLiteDatabase(Dictionary<String, String> connectionOpts)
        {
            log.Debug("Using options to connect to db: " + connectionOpts);
            String str = "";
            foreach (KeyValuePair<String, String> row in connectionOpts)
            {
                str += String.Format("{0}={1}; ", row.Key, row.Value);
            }
            str = str.Trim().Substring(0, str.Length - 1);
            dbConnection = str;
        }

        /// <summary>
        ///     Allows the programmer to run a query against the Database.
        /// </summary>
        /// <param name="sql">The SQL to run</param>
        /// <returns>A DataTable containing the result set.</returns>
        public DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SQLiteConnection cnn = new SQLiteConnection(dbConnection);
                cnn.Open();
                SQLiteCommand mycommand = new SQLiteCommand(cnn);
                
                //All subscribers to this method use parametized inputs so we're safe
                mycommand.CommandText = sql;

                try
                {
                    SQLiteDataReader reader = mycommand.ExecuteReader();
                    dt.Load(reader);
                    reader.Close();
                }
                catch (Exception ex1)
                {
                    log.Fatal("Error getting table data", ex1);
                }
                finally
                {                    
                    cnn.Close();
                }
            }
            catch (Exception ex1)
            {
                log.Fatal("Error getting table data " + sql, ex1);
            }

            log.Info("GetDataTable:DataTable|dt|:: " + dt);
            return dt;
        }

        /// <summary>
        ///     Allows the programmer to interact with the database for purposes other than a query.
        /// </summary>
        /// <param name="sql">The SQL to be run.</param>
        /// <returns>An Integer containing the number of rows updated. Return -1 indicates error</returns>
        public int ExecuteNonQuery(string sql)
        {
            log.Debug("Executing nonQuery " + sql);

            int rowsUpdated = -1;
            SQLiteConnection cnn = new SQLiteConnection(dbConnection);
            cnn.Open();

            try
            {
                SQLiteCommand mycommand = new SQLiteCommand(cnn);
                //All subscribers to this method use parametized inputs so we're safe
                mycommand.CommandText = sql;
                rowsUpdated = mycommand.ExecuteNonQuery();

            } catch (Exception ex1)
            {
                log.Fatal("Error executing non-query " + sql, ex1);
            }
            finally
            {
                cnn.Close();
            }
            return rowsUpdated;
        }

        /// <summary>
        ///     Allows the programmer to retrieve single items from the DB.
        /// </summary>
        /// <param name="sql">The query to run.</param>
        /// <returns>A string.</returns>
        public string ExecuteScalar(string sql)
        {
            log.Debug("ExecuteScalar:sql:: " + sql);

            object value = null;
            SQLiteConnection cnn = new SQLiteConnection(dbConnection);
            cnn.Open();
            SQLiteCommand mycommand = new SQLiteCommand(cnn);
            //All subscribers to this method use parametized inputs so we're safe
            mycommand.CommandText = sql;

            try
            {
                value = mycommand.ExecuteScalar();
            }
            catch (Exception ex1)
            {
                log.Fatal("Error executing scalar " + sql, ex1);
            }
            finally
            {
                cnn.Close();
            }

            if (value != null)        
                return value.ToString();  
          
            return "";
        }

        /// <summary>
        ///     Allows the programmer to easily update rows in the DB.
        /// </summary>
        /// <param name="tableName">The table to update.</param>
        /// <param name="data">A dictionary containing Column names and their new values.</param>
        /// <param name="where">The where clause for the update statement.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Update(String tableName, Dictionary<String, String> data, String where)
        {      

            String vals = "";
            Boolean returnCode = true;
            if (data.Count >= 1)
            {
                foreach (KeyValuePair<String, String> val in data)
                {
                    vals += String.Format(" {0} = '{1}',", val.Key.ToString(), val.Value.ToString());
                }
                vals = vals.Substring(0, vals.Length - 1);

                log.Debug(String.Format("update {0} set {1} where {2};", tableName, vals, where));
            }
            try
            {
                this.ExecuteNonQuery(String.Format("update {0} set {1} where {2};", tableName, vals, where));
            }
            catch (Exception ex1)
            {
                returnCode = false;
                log.Fatal("Failed to update table with query: " + String.Format("update {0} set {1} where {2};", tableName, vals, where), ex1);
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily delete rows from the DB.
        /// </summary>
        /// <param name="tableName">The table from which to delete.</param>
        /// <param name="where">The where clause for the delete.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Delete(String tableName, String where)
        {

            log.Debug(String.Format("delete from {0} where {1};", tableName, where));

            Boolean returnCode = true;
            try
            {
                this.ExecuteNonQuery(String.Format("delete from {0} where {1};", tableName, where));
            }
            catch (Exception ex1)
            {              
                returnCode = false;
                log.Fatal("Failed to Delete with query: " + String.Format("delete from {0} where {1};", tableName, where), ex1);
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily insert into the DB
        /// </summary>
        /// <param name="tableName">The table into which we insert the data.</param>
        /// <param name="data">A dictionary containing the column names and data for the insert.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Insert(String tableName, Dictionary<String, String> data)
        {
            String columns = "";
            String values = "";
            Boolean returnCode = true;
            foreach (KeyValuePair<String, String> val in data)
            {
                columns += String.Format(" {0},", val.Key.ToString());
                values += String.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);

            log.Debug("Inserting with query" + String.Format("insert into {0}({1}) values({2});", tableName, columns, values));

            try
            {
                this.ExecuteNonQuery(String.Format("insert into {0}({1}) values({2});", tableName, columns, values));
            }
            catch (Exception ex1)
            {
                returnCode = false;
                log.Fatal("Error inserting with query " + String.Format("insert into {0}({1}) values({2});", tableName, columns, values), ex1);
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily delete all data from the DB.
        /// </summary>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ClearDB()
        {
            log.Debug("Clearing DB");
            DataTable tables;
            try
            {
                tables = this.GetDataTable("select NAME from SQLITE_MASTER where type='table' order by NAME;");
                foreach (DataRow table in tables.Rows)
                {
                    this.ClearTable(table["NAME"].ToString());
                }
                return true;
            }
            catch (Exception ex1)
            {
                log.Fatal("Error clering db", ex1);
                return false;
            }
        }

        /// <summary>
        ///     Allows the user to easily clear all data from a specific table.
        /// </summary>
        /// <param name="table">The name of the table to clear.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ClearTable(String table)
        {
            log.Debug("Clearing table " + table);
            try
            {

                this.ExecuteNonQuery(String.Format("delete from {0};", table));
                return true;
            }
            catch( Exception ex1)
            {
                log.Fatal("Error clearing table with " + String.Format("delete from {0};", table), ex1);
                return false;
            }
        }
    }

}