﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;

namespace BitFeeder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private System.Windows.Forms.NotifyIcon _notifyIcon;
        private System.Windows.Forms.ContextMenu _contextMenu;


        private readonly FeedManager manager = new FeedManager();
        private readonly FeedShardElement<FeedShard> ticker;

        private delegate void FeedDelegate(FeedItem itm);

        // I think this height looks the best... no other reason :)
        private const int FULL_HEIGHT = 60;
        private const int SHORT_HEIGHT = 0;

        private int height = FULL_HEIGHT;

        public MainWindow()
        {
            InitializeComponent();
            initNotifyIcon();
            
            DataContext = FeedAmalgam.Instance;

            ticker = new FeedShardElement<FeedShard>(LayoutRoot);
            manager.NewFeedItem += manager_NewFeedItem;
        }

        private void manager_NewFeedItem(object sender, FeedEventArgs<FeedItem> e)
        {

            //Got a new feeditem. Marshall to the UI thread
            Dispatcher.Invoke(DispatcherPriority.Background, new FeedDelegate(AddItem), e.Item);
            height = FULL_HEIGHT;
            Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(() => Height = height));
        }


        private void AddItem(FeedItem itm)
        {
            lock (this)
            {
                ticker.Items.Enqueue(new FeedShard(itm));
                FeedAmalgam.Instance.TotalFeedCount++;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            manager.Start();
        }

        private void amalgamControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {        
            WindowState = WindowState.Minimized;
            _notifyIcon.BalloonTipText = "";
        }

        void OnClose(object sender, CancelEventArgs args)
        {
            _notifyIcon.Dispose();
            _notifyIcon = null;
        }

        #region NotifyIcon

        public void initNotifyIcon()
        {
            // Add menu items to context menu.
            _contextMenu = new ContextMenu();
            _contextMenu.MenuItems.Add("&Open");
            _contextMenu.MenuItems.Add("&Mute");
            _contextMenu.MenuItems.Add("&Settings");
            _contextMenu.MenuItems.Add("&Exit");

            _notifyIcon = new System.Windows.Forms.NotifyIcon();
            _notifyIcon.BalloonTipText = "Bitfeeder has been minimized. Click the tray icon to show.";
            _notifyIcon.BalloonTipTitle = "BitFeeder";
            _notifyIcon.Text = "BitFeeder";
            _notifyIcon.Icon = Properties.Resources.bitfeeder;
            _notifyIcon.Click += new EventHandler(_notifyIcon_Click);

            _notifyIcon.ContextMenu = _contextMenu;
        }


        private WindowState m_storedWindowState = WindowState.Normal;
        void OnStateChanged(object sender, EventArgs args)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
                if (_notifyIcon != null)
                    _notifyIcon.ShowBalloonTip(2000);
            }
            else
                m_storedWindowState = WindowState;
        }
        void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            CheckTrayIcon();
        }

        void _notifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = m_storedWindowState;
        }
        void CheckTrayIcon()
        {
            ShowTrayIcon(!IsVisible);
        }

        void ShowTrayIcon(bool show)
        {
            if (_notifyIcon != null)
                _notifyIcon.Visible = show;
        }

        #endregion

        #region WindowMovement
        private void amalgamControl_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {                      
            this.DragMove();
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            System.Drawing.Rectangle primaryBounds = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            System.Drawing.Rectangle windowBounds = new System.Drawing.Rectangle(Convert.ToInt32(this.Left), Convert.ToInt32(this.Top), Convert.ToInt32(this.Width), Convert.ToInt32(this.Height));

            if ((windowBounds.Left < 0))
            {
                windowBounds = new System.Drawing.Rectangle(0, windowBounds.Top, windowBounds.Width, windowBounds.Height);
            }
            else if ((windowBounds.Right > primaryBounds.Right))
            {
                windowBounds = new System.Drawing.Rectangle(primaryBounds.Right - windowBounds.Width, windowBounds.Top, windowBounds.Width, windowBounds.Height);
            }

            if ((windowBounds.Top < 0))
            {
                windowBounds = new System.Drawing.Rectangle(windowBounds.Left, 0, windowBounds.Width, windowBounds.Height);
            }
            else if ((windowBounds.Bottom > primaryBounds.Bottom))
            {
                windowBounds = new System.Drawing.Rectangle(windowBounds.Left, primaryBounds.Bottom - windowBounds.Height, windowBounds.Width, windowBounds.Height);
            }

            this.Left = windowBounds.Left;
            this.Top = windowBounds.Top;

        }

        #endregion

        #region KeyListeners

        private void HandleKeyDownEvent(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Drawing.Rectangle primaryBounds = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            System.Drawing.Rectangle windowBounds = new System.Drawing.Rectangle(Convert.ToInt32(this.Left), Convert.ToInt32(this.Top), Convert.ToInt32(this.Width), Convert.ToInt32(this.Height));

            if (e.Key == Key.A && (Keyboard.Modifiers & (ModifierKeys.Control | ModifierKeys.Shift)) == (ModifierKeys.Control | ModifierKeys.Shift))
            {
                // Align window with top edge of screen
                windowBounds = new System.Drawing.Rectangle(windowBounds.Left, 0, windowBounds.Width, windowBounds.Height);                
            }

            if (e.Key == Key.D && (Keyboard.Modifiers & (ModifierKeys.Control | ModifierKeys.Shift)) == (ModifierKeys.Control | ModifierKeys.Shift))
            {
                // Align window with botton, just over task bar
                windowBounds = new System.Drawing.Rectangle(windowBounds.Left, (primaryBounds.Bottom - windowBounds.Height) 
                    - (Screen.PrimaryScreen.Bounds.Height - Screen.PrimaryScreen.WorkingArea.Height), windowBounds.Width, windowBounds.Height);
            }

            this.Left = windowBounds.Left;
            this.Top = windowBounds.Top;
        }

        #endregion

    }
}
