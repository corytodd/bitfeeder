﻿using log4net;
using System;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;


namespace BitFeeder
{
    /// <summary>
    /// Interaction logic for FeedShard.xaml
    /// </summary>
    public partial class FeedShard : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FeedShard(FeedItem item)
        {
            InitializeComponent();

            log.Info("Creating feed shard for feed item " + item.ToString());

            FeedItem = item;

            txtDescription.Text = String.IsNullOrEmpty(item.Description) ? "" : item.Description.ToString();
            txtTitle.Text = item.Title;

            if (item.DirectLink.ToLower().Trim().StartsWith("http"))
            {
                try
                {
                    hlLink.NavigateUri = new Uri(item.DirectLink);
                }
                catch(Exception ex1)
                {
                    log.Fatal("Error creating feedChunk for item", ex1);
                }
            }
            txtDate.Text = DateTime.Parse(item.Date).ToString();
        }

        public FeedItem FeedItem { get; private set;}
        
        public string PubDate
        {
            get
            {
                TextBlock date = (TextBlock)FindName("txtDate");
                return date.Text;
            }
        }
        
        public string Title
        {
            get
            {
                return txtTitle.Text;
            }
        }

        public string Url
        {
            get
            {
                return hlLink.NavigateUri.ToString();
            }
        }

        public string Description
        {
            get
            {
                return txtDescription.Text;
            }
        }
        
        private void hpLink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.ToString()));
            e.Handled = true;        
        }
    }    
}

