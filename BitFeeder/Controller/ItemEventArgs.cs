﻿using System;

namespace BitFeeder
{
    public class FeedEventArgs<T> : EventArgs
    {
        public FeedEventArgs(T item)
        {
            Item = item;
        }

        public T Item { get; private set; }
    }
}