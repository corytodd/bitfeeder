﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace BitFeeder
{
    public class FeedManager
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private bool stop;
        private BitFeedDB db = new BitFeedDB();
        private List<string> feeds = new List<string>();        
        private Dictionary<string, FeedItem> feedItems = new Dictionary<string, FeedItem>();

        //Timer is used to fetch new news feeds. Default is every 15 minutes.
        public DispatcherTimer t = new DispatcherTimer();

        //Event provides a callback to inform the UI there is a new feed.
        //this is called once per article
        public event EventHandler<FeedEventArgs<FeedItem>> NewFeedItem;

        public FeedManager()
        {
            t.Tick += (o, e) => Go();
            t.Interval = new TimeSpan(0, 15, 0);
        }

        public void Start()
        {
            stop = false;
            Go();
            t.Start();
        }

        public void Stop()
        {
            t.Stop();
            stop = true;
        }

        private void Go()
        {
            // Launch Go in its own thread
            Task.Factory.StartNew(GoTask);
        }

        // Runs every 15 minutes by default. Fetches all RSS feeds and launches parser threads for each.
        private void GoTask()
        {
            feeds = db.GetAllFeeds();

            List<Task> tasks = new List<Task>();

            //Once thread per chunk            
            foreach (string s in feeds)
            {
                if (stop) return;                
                tasks.Add(Task.Factory.StartNew(ParseFeed, s));
            }

            // Wait on all parsing tasks
            Task.WaitAll(tasks.ToArray());

            // Okay, we're done parsing all the feeds. Check for dupes...
            List<FeedItem> freshItems = feedItems.Values.Where(x => CheckIsNew(x)).ToList<FeedItem>();

            // Finally, add all the freshItems to the db
            foreach (FeedItem item in freshItems)
                db.AddNewFeedItem(item);    

           
            // All Disk IO should be done so start the UI feed
            foreach (FeedItem item in freshItems)
            {
                if (stop) return;

                Task.Factory.StartNew(launchTicker, item);
            }              

            // Dump our dictionary so as to not waste memory holding on to it.
            feeds.Clear();
        }

        // Only called in a thread from Go()
        private void ParseFeed(object obj)
        {
            try
            {
                string s = (string) obj;

                IFeedParser parser = ParserFactory.GetParser(s);

                if (parser == null) return;

                List<FeedItem> parse = parser.Parse(s);

                if (parse == null) return;

                foreach (FeedItem item in parse)
                {
                    if (stop) return;
                    lock (feedItems)
                    {
                        try
                        {
                            // Clean and tag the item
                            BitFeedDB.CleanItem(item);
                            BitFeedDB.Categorize(item);

                            feedItems.Add(item.Guid, item);
                        }
                        catch (ArgumentException)
                        {
                            
                            log.Info("BitFeeder.FeedManager::ParseFeed > FeedItem is broken" + item.ToString());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Fatal("BitFeeder.FeedManager::ParseFeed > Server Error" + e);
            }        
        }

        // Only start an animation for entries that have not been written to the database.
        private bool CheckIsNew(object item)
        {
            FeedItem feedItem = (FeedItem)item;
            bool isNew = false;

            if (!db.IsStaleItem(feedItem.Date, feedItem.Description))
            {
                log.Debug(String.Format("Item {0} is stale", item.ToString()));
                isNew = true;
            }

            
            return isNew;
        }

        private void launchTicker(object item)
        {
            FeedItem feedItem = (FeedItem)item;
            if (NewFeedItem != null) NewFeedItem(this, new FeedEventArgs<FeedItem>(feedItem));
        }
    }
}