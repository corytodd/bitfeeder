﻿using log4net;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;


namespace BitFeeder
{
    class FeedShardElement<T> where T : FrameworkElement
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly DispatcherTimer displayTimer = new DispatcherTimer();
        public bool Running { get; set; }
        public double SeperatorSize { get; set; }

        // Set the speed of the shard scroller
        public TimeSpan Speed { get; set; }
        public Queue<T> Items { get; set; }
        public Panel Container { get; private set; }

        private FrameworkElement ee;

        /// <summary>
        /// Immediately stop any feed banner scrolling
        /// </summary>
        public void Stop()
        {
            displayTimer.Stop();
            Running = false;
        }

        /// <summary>
        /// Immediately start any feed banner scrolling
        /// </summary>
        public void Start()
        {
            displayTimer.Start();
            displayTimer.Interval = new TimeSpan(0,0,0,1);
            Running = true;
        }

        /// <summary>
        /// Create a new container for a feed item shard. There are 25 pixels between each element.
        /// Each element runs for 10 seconds across the parent container.
        /// </summary>
        /// <param name="container"></param>
        public FeedShardElement(Panel container)
        {
            SeperatorSize = 25;

            Container = container;

            log.Debug("Creating shard with size: " + Container.DesiredSize.Width);

            Speed = new TimeSpan(0, 0, 0, 10);

            Items = new Queue<T>();

            displayTimer.Tick += displayTimer_Tick;
            displayTimer.Start();
            Running = true;
        }
        
        /// <summary>
        /// Callback for the element feed timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void displayTimer_Tick(object sender, EventArgs e)
        {
            DisplayNextItem();
        }

        /// <summary>
        /// Launches the next FeedShardElement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayNextItem()
        {
            if (Items.Count == 0) return;

            T item = Items.Dequeue();
            FeedAmalgam.Instance.TotalFeedCount--;

            Container.Children.Add(item);

            AnimateMove(item);

        }

        /// <summary>
        /// Use DoubleAnimation to move this element across the parent container
        /// within the time specified.
        /// </summary>
        /// <param name="e"></param>
        private void AnimateMove(FrameworkElement e)
        {
            ee = e;

            e.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
            e.Arrange(new Rect(e.DesiredSize));

            
            Container.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
            Container.Arrange(new Rect(Container.DesiredSize));

            double from = Container.ActualWidth;
            // The counter is 1/30th the parent container
            double to = from /30;
            
            int unitsPerSec = Convert.ToInt32(Math.Abs(from - to)/Speed.TotalSeconds);
            int nextFire = Convert.ToInt32((e.ActualWidth + SeperatorSize)/unitsPerSec);

            displayTimer.Stop();
            displayTimer.Interval = new TimeSpan(0, 0, nextFire);
            displayTimer.Start();

            ShardDoubleAnimation ani = new ShardDoubleAnimation
                                            {
                                                From = from, 
                                                To = to, 
                                                Duration = new Duration(Speed), 
                                                FeedChunk = e                                                
                                            };

            TranslateTransform trans = new TranslateTransform();
            e.RenderTransform = trans;

            trans.BeginAnimation(TranslateTransform.XProperty, ani, HandoffBehavior.Compose);            
        }

    }
}
